//
//  ViewController.swift
//  Dicee
//
//  Created by Ahmed adham on 1/6/18.
//  Copyright © 2018 Ahmed adham. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var randomDiceeIndex1 : Int = 0
    var randomDiceeIndex2 : Int = 0
    let arrayDiceeName = ["dice1","dice2","dice3","dice4","dice5","dice6"]

    @IBOutlet weak var imageDicee1: UIImageView!
    
    @IBOutlet weak var imageDicee2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateImageDice()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnRoll(_ sender: UIButton) {
        

        updateImageDice()
        
    }
    
    func updateImageDice () {
        
        randomDiceeIndex1 = Int(arc4random_uniform(6))
        randomDiceeIndex2 = Int(arc4random_uniform(6))
        
        imageDicee1.image = UIImage(named: arrayDiceeName[randomDiceeIndex1])
        imageDicee2.image = UIImage(named: arrayDiceeName[randomDiceeIndex2])  }

    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
    
        updateImageDice()
        
    }
}

